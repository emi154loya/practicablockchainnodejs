//Este va a ser nuestro modelo de como va a funcionar nuestra cadena de bloques

const SHA256 = require("crypto-js/sha256");

class Block {
  //Se definen los atributos basicos de un bloque que se utiliza en la cadena de bloques:
  // index: Posicion del bloque en la cadean (donde estoy)
  // Data: Define el contenido dela cadena (Hash, posicion y la informacion que guarde)
  // previousHash: Es el valo del bloque anterior de la cadena encriptado o cifrado.
  // date: fecha de la creacion del bloque
  // hash: la validacion como cadena valida.
  // nounce: numero aleatorio de la cadena (solo por este caso).

  constructor(index, data, previousHash = "") {
    this.index = index;
    this.data = data;
    this.previousHash = previousHash;

    this.nounce = 0;
    this.hash = this.createdHash();
  }

  createdHash() {
    //Un hash es una cadena alfanumerica que es el resultado de usar una tecnica de encriptacion con un mensaje.
    //Aqui se utiliza la libreria cryptoJs

    const originalChain = `${this.index}|${this.data}|${this.date}|${this.nounce}`;
    return SHA256(originalChain).toString();
  }

  mine(difficulty) {
    while (!this.hash.startsWith(difficulty)) {
      this.nounce++;
      this.hash = this.createdHash();
    }
  }
}

module.exports = Block;
