Es una estructura de datos, que el bloque actual encripta el bloque anterior y coloca como bloque siguiente el bloque encriptado.

Esta estructura tiene muchas aplicaciones como;
    - Criptomonedas.
    - Contratos inteligentes: estos son contratos que no se pueden modificar por ninguna de las partes y que son validados por terceros al azar.
    - Libros de contabilidad: Cuando se quiere colocar información, cada movimiento hecho lo anexas a la cadena de bloques y guardas los saldos, esto para detectar inconsistencias.

Comandos para poder hacer hacer blockchain como un projecto de node:

emi154loya@Emiliano:~/Escritorio/clases/Parcial2/Clases/blockchain$ npm init
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help init` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (blockchain) 
version: (1.0.0) 
description: Ejemplo de una cadena de bloques
entry point: (index.js) 
test command: 
git repository: 
keywords: blockchain, demo, uach
author: Emiliano Loya Flores
license: (ISC) 
About to write to /home/emi154loya/Escritorio/clases/Parcial2/Clases/blockchain/package.json:

{
  "name": "blockchain",
  "version": "1.0.0",
  "description": "Ejemplo de una cadena de bloques",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [
    "blockchain",
    "demo",
    "uach"
  ],
  "author": "Emiliano Loya Flores",
  "license": "ISC"
}


Is this OK? (yes) yes
